﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exercise5
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = new string[5] { "red", "blue", "orange", "white", "black" };
            Array.Sort(a);
            Array.Reverse(a);
            Console.WriteLine(string.Join(", ", a));
        }
    }
}
